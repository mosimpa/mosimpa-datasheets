# Hojas de datos relacionadas al proyecto MoSimPa

Repositorio para tener las hojas de datos clasificadas y tagueadas.
# Listado de hojas de datos

| Código Mosimpa | Value                             | Tipo               | Encapsulado          | Número de Parte               | Link a la hoja de datos                                            |
| -------------- | --------------------------------- | ------------------ | -------------------- | ----------------------------- | ------------------------------------------------------------------ |
| C_00001        | 0.1uF                             | Capacitor          | 0603                 | CC0603JRX7R8BB104             | [CC0603JRX7R8BB104](C/CC0603JRX7R8BB104.pdf)                       |
| C_00002        | 10uF                              | Capacitor          | 0603                 | CM105X5R106M25AT              | [CM105X5R106M25AT](C/CM105X5R106M25AT.pdf)                         |
| C_00003        | 1uF                               | Capacitor          | 0603                 | GRM185C81A105KE36D            | [GRM185C81A105KE36D](C/GRM185C81A105KE36D.pdf)                     |
| C_00004        | 22uF                              | Capacitor          | 0805                 | GRM21BR61E226ME44K            | [GRM21BR61E226ME44K](C/GRM21BR61E226ME44K.pdf)                     |
| C_00008        | 47nF                              | Capacitor          | 0                    | C0603C472J5RACTU              | [C0603C472J5RACTU](C/C0603C472J5RACTU.pdf)                         |
| C_00009        | 47pF                              | Capacitor          | 0                    | CC0201JRNPO9BN470             | [CC0201JRNPO9BN470](C/CC0201JRNPO9BN470.pdf)                       |
| C_00010        | 10pF                              | Capacitor          | 0                    | GRM0335C1H100JA01D            | [GRM0335C1H100JA01D](C/GRM0335C1H100JA01D.pdf)                     |
| C_00011        | 1nF                               | Capacitor          | 0                    | GRM1885C1H102JA01D            | [GRM1885C1H102JA01D](C/GRM1885C1H102JA01D.pdf)                     |
| D_00001        | 2A                                | Schottky Diode     | 0603                 | SDM2U40CSP-7B                 | [SDM2U40CSP-7B](D/SDM2U40CSP-7B.pdf)                               |
| D_00002        | Green                             | LED                | 0603                 | LTST-S270KGKT                 | [LTST-S270KGKT](D/LTST-S270KGKT.pdf)                               |
| D_00003        | Red                               | LED                | 0603                 | LTST-C191KRKT                 | [LTST-C191KRKT](D/LTST-C191KRKT.pdf)                               |
| D_00004        | Blue                              | LED                | 0603                 | 19-213SUBC/S400-A5/S208-2/TR8 | [19-213SUBC/S400-A5/S208-2/TR8](D/19-213SUBC:S400-A5:S208-2:TR8.pdf)|
| L_00001        | 4.7uH                             | Inductor           | MDWK4040T4R7MMV      | MDWK4040T4R7MMV               | [MDWK4040T4R7MMV](L/MDWK4040T4R7MMV.pdf)                           |
| M_00001        | ESP32-WROOM-32D (16MB)            | ESP32-WROOM-32     | ESP32-WROOM-32       | ESP32-WROOM-32D (16MB)        | [ESP32-WROOM-32D (16MB)](esp/ESP32-WROOM-32D (16MB).pdf)           |
| NB_0001        | Hirose 1.25mm Pitch PCB RA        | Conector SMD 6p    | 6 posiciones         | DF13-6P-1.25H(75)             | [DF13-6P-1.25H(75)](N/DF13_.pdf)                                     |
| NB_0002        | Hirose 1.25mm Pitch PCB RA        | Conector SMD 5p    | 5 posiciones         | DF13-5P-1.25H(75)             | [DF13-5P-1.25H(75)](N/DF13_.pdf)                                     |
| NB_0003        | Hirose 1.25mm Pitch PCB RA        | Conector SMD 4p    | 4 posiciones         | DF13-4P-1.25H(75)             | [DF13-4P-1.25H(75)](N/DF13_.pdf)                                     |
| NC_0001        | Hirose 1.25mm Pitch Cable Housing | Conector Cable 6p  | 6 posiciones         | DF13-6S-1.25C                 | [DF13-6S-1.25C](N/DF13_.pdf)                                         |
| NC_0002        | Hirose 1.25mm Pitch Cable Housing | Conector Cable 5p  | 5 posiciones         | DF13-5S-1.25C                 | [DF13-5S-1.25C](N/DF13_.pdf)                                         |
| NC_0003        | Hirose 1.25mm Pitch Cable Housing | Conector Cable 4p  | 4 posiciones         | DF13-4S-1.25C                 | [DF13-4S-1.25C](N/DF13_.pdf)                                         |
| NP_0001        | Hirose 1.25mm Pitch Cable Housing | Pin Cable          | 1                    | DF13-2630SCF                  | [DF13-2630SCF](N/DF13_.pdf)                                          |
| NP_0002        | Terminal SLDRT                    | 0                  | 1                    | 172249-0200                   | [172249-0200](N/172249-0200.pdf)                                     |
| Q_00001        | MOSFET-P                          | Mosfet P           | SOT-23-3L_FAS        | FDV302P                       | [FDV302P](Q/FDV302P.pdf)                                             |
| Q_00002        | MOSFET-N                          | Mosfet N           | SOT-23-3L_FAS        | FDV301N                       | [FDV301N](Q/FDV301N.pdf)                                             |
| R_00001        | 0ohm                              | Jumper             | 0603                 | CRCW06030000Z0EC              | [CRCW06030000Z0EC](R/CRCW06030000Z0EC.pdf)                           |
| R_00002        | 10K                               | Resistor           | 0603                 | ERJ-3EKF1002V                 | [ERJ-3EKF1002V](R/ERJ-3EKF1002V.pdf)                                 |
| R_00003        | 1K                                | Resistor           | 0603                 | ERA-3AEB102V                  | [ERA-3AEB102V](R/ERA-3AEB102V.pdf)                                   |
| R_00004        | 10ohm                             | Resistor           | 0603                 | CRCW060310R0FKEA              | [CRCW060310R0FKEA](R/CRCW060310R0FKEA.pdf)                           |
| R_00005        | 4.7K                              | Resistor           | 0603                 | RMCF0603FT4K70                | [RMCF0603FT4K70](R/RMCF0603FT4K70.pdf)                               |
| R_00006        | 1.2K                              | Resistor           | 0603                 | CRCW06031K20FKEA              | [CRCW06031K20FKEA](R/CRCW06031K20FKEA.pdf)                           |
| R_00010        | 200ohm                            | Resistor           | 0                    | SFR03EZPF2000                 | [SFR03EZPF2000](R/SFR03EZPF2000.pdf)                                 |
| R_00011        | 324ohm 0.5%                       | Resistor           | 0                    | ERJ-PB3D3240V                 | [ERJ-PB3D3240V](R/ERJ-PB3D3240V.pdf)                                 |
| S_00001        | Switch SMD                        | Switch SMD         | PTS645SM43SMTR92LFS  | PTS645SM43SMTR92LFS           | [PTS645SM43SMTR92LFS](S/PTS645SM43SMTR92LFS.pdf)                     |
| U_00001        | Reg 3.3V                          | Regulator          | 8-SOIC               | MCP1725-3302E/SN              | [MCP1725-3302E/SN](U/MCP1725-3302E/SN.pdf)                           |
| U_00002        | RS232Driver                       | IC                 | 16-SSOP              | MAX3221ECAE+T                 | [MAX3221ECAE+T](U/MAX3221ECAE+T.pdf)                                 |
| U_00003        | Proc.OX                           | IC                 | TQFN-24              | MAX32664GTGA+                 | [MAX32664GTGA+](U/MAX32664GTGA+.pdf)                                 |
| U_00004        | LVL-SH                            | IC                 | 14-SOIC              | TXS0104ED                     | [TXS0104ED](U/TXS0104ED.pdf)                                         |
| U_00005        | Inst_Amp                          | I_AMP              | 8-VSSOP              | INA332AIDGKR                  | [INA332AIDGKR](U/INA332AIDGKR.pdf)                                   |
| U_00006        | OPAM                              | OPAM               | SOT-23-5             | OPA340NA/3K                   | [OPA340NA/3K](U/OPA340NA/3K.pdf)                                     |
| U_00007        | SW-V-Reg                          | Regulator          | TSOT-26-6            | AP63205WU-7                   | [AP63205WU-7](U/AP63205WU-7.pdf)                                     |
| U_00008        | LiON Ch                           | Charger            | 10-VFDFN Exposed Pad | MCP73213-A6SI/MF              | [MCP73213-A6SI/MF](U/MCP73213-A6SI/MF.pdf)                           |
| U_00009        | Reg 1.8V                          | Regulator          | 8-SOIC               | MCP1725-1802E/SN              | [MCP1725-1802E/SN](U/MCP1725-1802E/SN.pdf)                           |
| U_00010        | Ox Sensor                         | Ox Sensor          | -                    | MAX30101EFD+T                 | [MAX30101EFD+T](U/MAX30101EFD+T.pdf)                                 |
| U_00011        | Accelerometer                     | Accelerometer      | LGA-14               | LIS2DHTR                      | [LIS2DHTR](U/LIS2DHTR.pdf)                                           |
| U_00013        | Temp Sensor                       | Temp Sensor        | THIN LGA             | MAX30208CLB+                  | [MAX30208CLB+](U/MAX30208CLB+.pdf)                                   |
| U_00017        | BioZ/ECG                          | BioZ/ECG           | 30WLP                | MAX30002CWV+T                 | [MAX30002CWV+T](U/MAX30002CWV+T.pdf)                                 |
| Y_00001        | Xtal: 32.768KH                    | Crystal Oscillator | 2-SMD, No Lead       | ABS07-120-32.768KHZ-T         | [ABS07-120-32.768KHZ-T](Y/ABS07-120-32.768KHZ-T.pdf)                 |

# Otros documentos

## MAX32664

- [MAX32664 user guide](U/user-guide-6806-max32664.pdf)
